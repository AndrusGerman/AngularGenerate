package main

func generateByTable(data Tabla) (imp string, declaration string) {
	// Create Folder
	eCreateFolder(packageName + "/" + eP(data.TableNameSingular) + "/add-" + eP(data.TableNameSingular))
	eCreateFolder(packageName + "/" + eP(data.TableNameSingular) + "/all-" + eP(data.TableNameSingular))
	eCreateFolder(packageName + "/" + eP(data.TableNameSingular) + "/edit-" + eP(data.TableNameSingular))
	eCreateFolder(packageName + "/" + eP(data.TableNameSingular) + "/view-" + eP(data.TableNameSingular))
	// View Create
	createView(data)
	// Add Create
	createAdd(data)
	// All Create
	createAll(data)
	// Edit Create
	createEdit(data)
	// Create Model
	createModel(data)
	// Create Service
	createService(data)
	imp = `
import { Edit` + data.TableNameModel + `Component } from './` + eP(data.TableNameSingular) + `/edit-` + eP(data.TableNameSingular) + `/edit-` + eP(data.TableNameSingular) + `.component';
import { View` + data.TableNameModel + `Component } from './` + eP(data.TableNameSingular) + `/view-` + eP(data.TableNameSingular) + `/view-` + eP(data.TableNameSingular) + `.component';
import { Add` + data.TableNameModel + `Component } from './` + eP(data.TableNameSingular) + `/add-` + eP(data.TableNameSingular) + `/add-` + eP(data.TableNameSingular) + `.component';
import { All` + data.TableNameModel + `Component } from './` + eP(data.TableNameSingular) + `/all-` + eP(data.TableNameSingular) + `/all-` + eP(data.TableNameSingular) + `.component';	
`
	//
	declaration = `
    Edit` + data.TableNameModel + `Component,
    View` + data.TableNameModel + `Component,
    Add` + data.TableNameModel + `Component,
    All` + data.TableNameModel + `Component,
`
	return imp, declaration
}
