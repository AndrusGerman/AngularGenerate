package main

func createAdd(table Tabla) {
	addscss(table)
	addHTML(table)
	addTS(table)
}

func addTS(table Tabla) {
	base, _ := eReadFileT("add", "ts")
	imports := eGetImportsFK(table.Valores)
	variables := eGetVariablesFK(table.Valores)
	onInit := eGetPrepareVarsFK(table.Valores)
	serviceConstructor := eGetServiceConstructorFK(table.Valores)
	base = eRP(base, "$[model]", table.TableNameModel)

	base = eRP(base, "$[import]", imports)
	base = eRP(base, "$[vars]", variables)
	base = eRP(base, "$[tablesingular]", table.TableNameSingular)
	base = eRP(base, "$[tablesingular-]", eP(table.TableNameSingular))
	base = eRP(base, "$[serviceconstructor]", serviceConstructor)
	base = eRP(base, "$[oninit]", onInit)
	base = eParseModelsService(base)
	eCreateFile(eNameTableFile(table, "ts", "add"), base)
}

func addHTML(table Tabla) {
	base, _ := eReadFileT("add", "html")
	form := htmAddGetForm(table.Valores)
	base = eRP(base, "$[data]", form)
	base = eRP(base, "$[model]", table.TableNameModel)
	base = eRP(base, "$[tablesingular-]", eP(table.TableNameSingular))
	eCreateFile(eNameTableFile(table, "html", "add"), base)
}

func addscss(table Tabla) {
	base, _ := eReadFileT("add", "scss")
	eCreateFile(eNameTableFile(table, "scss", "add"), base)
}
