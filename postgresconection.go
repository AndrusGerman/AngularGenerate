package main

import (
	"fmt"
	"os"

	"github.com/jinzhu/gorm"

	_ "github.com/jinzhu/gorm/dialects/postgres" // Postgres
)

//DbGetConnection obtienes la coneción de la Db
func DbGetConnection() *gorm.DB {
	fmt.Println("Conectando con la base de datos..")
	//Read config
	db, err := gorm.Open("postgres", "host="+DBhost+" port=5432 user="+DBUser+" dbname="+database+" password="+DBPassword+" sslmode=disable") //Postgres
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	return db
}
