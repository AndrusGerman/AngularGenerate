package main

import "fmt"

func createModel(table Tabla) {
	base, _ := eReadFile("base.ts")

	variables := ""
	for _, val := range table.Valores {
		variables += fmt.Sprintf("    public %s: %s;\n", val.ColName, val.DataType)
	}
	base = eRP(base, "$[model]", table.TableNameModel)
	base = eRP(base, "$[valores]", variables)
	base = eRP(base, "$[tablesingular-]", eP(table.TableNameSingular))

	fileName := "./" + packageName + "/" + eP(table.TableNameSingular) + "/" + eP(table.TableNameSingular) + ".ts"
	eCreateFile(fileName, base)
}
