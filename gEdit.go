package main

func createEdit(table Tabla) {
	editsCSS(table)
	editHTML(table)
	editTS(table)
}

func editTS(table Tabla) {
	base, _ := eReadFileT("edit", "ts")
	imports := eGetImportsFK(table.Valores)
	variables := eGetVariablesFK(table.Valores)
	onInit := eGetPrepareVarsFK(table.Valores)
	serviceConstructor := eGetServiceConstructorFK(table.Valores)
	base = eRP(base, "$[model]", table.TableNameModel)
	base = eRP(base, "$[import]", imports)
	base = eRP(base, "$[vars]", variables)
	base = eRP(base, "$[tablesingular]", table.TableNameSingular)
	base = eRP(base, "$[tablesingular-]", eP(table.TableNameSingular))
	base = eRP(base, "$[serviceconstructor]", serviceConstructor)
	base = eRP(base, "$[oninit]", onInit)
	base = eParseModelsService(base)
	eCreateFile(eNameTableFile(table, "ts", "edit"), base)
}

func editHTML(table Tabla) {
	base, _ := eReadFileT("edit", "html")
	form := htmAddGetForm(table.Valores)
	base = eRP(base, "$[data]", form)
	base = eRP(base, "$[tablesingular-]", eP(table.TableNameSingular))
	base = eRP(base, "$[model]", table.TableNameModel)
	eCreateFile(eNameTableFile(table, "html", "edit"), base)
}

func editsCSS(table Tabla) {
	base, _ := eReadFileT("edit", "scss")
	eCreateFile(eNameTableFile(table, "scss", "edit"), base)
}
