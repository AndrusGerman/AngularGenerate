package main

import (
	"fmt"

	"github.com/iancoleman/strcase"
	"github.com/jinzhu/gorm"
	"github.com/jinzhu/inflection"

	"strings"
)

//Tabla contiene los valores de la tabla
type Tabla struct {
	TableName         string
	TableNameModel    string
	TableNameSingular string
	Valores           []Valores
}

//Valores contiene sus valores
type Valores struct {
	DataType string
	ColName  string
	Fk       bool
	data     struct {
		Table         string
		TableSingular string
		Model         string
	}
}

func getTablesAndValues(Db *gorm.DB, schema string) []Tabla {
	fmt.Println("Descargando datos....")
	var tablas []Tabla
	//Busca todas las tablas
	Db.Raw("SELECT table_name FROM information_schema.tables WHERE table_schema = ? AND table_type = ?", schema, "BASE TABLE").Scan(&tablas)
	cdatos := len(tablas)
	fmt.Printf("00 - %d: Datos\n", cdatos)
	//busca los valores de esas tablas
	for ind, val := range tablas {
		if ind+1 <= 9 {
			fmt.Printf("0%d - %d: ", ind+1, cdatos)
		} else {
			fmt.Printf("%d - %d: ", ind+1, cdatos)
		}
		tablas[ind].TableNameModel = singularUperText(tablas[ind].TableName, "_")
		tablas[ind].TableNameSingular = inflection.Singular(tablas[ind].TableName)

		valores := []Valores{}
		Db.Raw(`SELECT attname AS col_name , atttypid::regtype  AS data_type
		FROM   pg_attribute
		WHERE  attrelid = '` + schema + `.` + val.TableName + `'::regclass  -- table name, optionally schema-qualified
		AND    attnum > 0
		AND    NOT attisdropped
		AND attname NOT LIKE 'created_at'
		AND attname NOT LIKE 'updated_at'
		AND attname NOT LIKE 'deleted_at'
		ORDER  BY attnum`).Scan(&valores)

		for IndValor, ValorValor := range valores {
			if strings.Contains(ValorValor.ColName, "Id") {
				valores[IndValor].ColName = strings.Replace(ValorValor.ColName, "Id", "ID", 1)

			}
			valores[IndValor].DataType = parseDataType(ValorValor.DataType)
			valores[IndValor].ColName = singularUperText(valores[IndValor].ColName, "_")

			if strings.Contains(valores[IndValor].ColName, "ID") && valores[IndValor].ColName != "ID" {
				d := strings.Replace(valores[IndValor].ColName, "ID", "", 1)
				valores[IndValor].Fk = true
				valores[IndValor].data.Table = strcase.ToSnake(inflection.Plural(d))
				valores[IndValor].data.TableSingular = strcase.ToSnake(d)
				valores[IndValor].data.Model = d
			}
		}
		tablas[ind].Valores = valores
		fmt.Printf("%s\n", tablas[ind].TableNameModel)
	}

	return tablas
}

func singularUperText(text string, spl string) string {
	if text == "id" {
		return "ID"
	}

	val := strings.Split(text, spl)
	textResul := ""
	for _, valorSpl := range val {
		if valorSpl == "id" {
			textResul += "ID"
		} else {
			singSpl := inflection.Singular(valorSpl)
			textResul += strings.Title(singSpl)
		}

	}

	return textResul

}

func parseDataType(DataType string) string {
	switch DataType {
	case "integer":
		return "number"
	case "numeric":
		return "number"
	case "character varying":
		return "string"
	case "timestamp without time zone":
		return "any"
	case "timestamp with time zone":
		return "any"
	case "text":
		return "string"
	case "boolean":
		return "boolean"
	default:
		return "any"
	}
}
