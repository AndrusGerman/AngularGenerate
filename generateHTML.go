package main

func htmAddGetForm(val []Valores) string {
	html := ""
	for _, dat := range eNV(val) {
		if dat.Fk == false {
			if dat.DataType == "Boolean" {
				// Es boolean
				memory := `
			<ion-col size-xs="12" size-sm="6">
        <ion-item>
          <ion-label position="floating">$[name]</ion-label>
          <ion-checkbox  checked="true" slot="end" [(ngModel)]="item.$[name]"></ion-checkbox>
        </ion-item>
			</ion-col>
`
				memory = eRP(memory, "$[name]", dat.ColName)
				html += memory
			} else if dat.DataType == "Date" {
				// Es Date
				memory := `
			<ion-col size-xs="12" size-sm="6">
        <ion-item>
          <ion-label position="floating">$[name]</ion-label>
					<input type="date" value="2019-03-21" min="2019-03-21" [(ngModel)]="item.$[name]"/>
        </ion-item>
			</ion-col>
`
				memory = eRP(memory, "$[name]", dat.ColName)
				html += memory
			} else if dat.ColName != "Image" {
				// No es FK no es Image
				memory := `
			<ion-col size-xs="12" size-sm="6">
            <ion-item>
              <ion-label position="floating">
                <ion-icon slot="start" name="clipboard"></ion-icon>
                $[name]
              </ion-label>
              <ion-input [(ngModel)]="item.$[name]" $[type]></ion-input>
            </ion-item>
			</ion-col>
			`
				memory = eRP(memory, "$[name]", dat.ColName)
				if dat.DataType == "string" || dat.DataType == "any" {
					memory = eRP(memory, "$[type]", "")
				} else {
					memory = eRP(memory, "$[type]", `type="number"`)
				}
				html += memory
			} else if (dat.ColName == "Image") || (dat.ColName == "Logo") {
				// No es Fk es Image
				memory := `
			<ion-col style="mouse" size-xs="12" size-sm="6">
				<ion-item>
					<ion-label (click)="press_button('button-` + dat.ColName + `')" position="floating">
						<ion-icon slot="start" name="camera"></ion-icon>
						` + dat.ColName + `: '{{` + dat.ColName + `?.name}}'
					</ion-label>
				</ion-item>
				<input id="button-` + dat.ColName + `" class="invisible" type="file" accept="image/*" (change)="change` + dat.ColName + `($event)" />
			</ion-col>
			`
				html += memory
			}
		} else {
			// Es FK
			memory := `   
		<ion-col size-xs="12" size-sm="6">
            <ion-item>
              <ion-label position="floating">
                <ion-icon slot="start" name="logo-buffer"></ion-icon>
                $[name]
              </ion-label>
              <ion-select [(ngModel)]="item.$[name]ID" okText="Aceptar" cancelText="Cancelar">
                <div *ngFor="let elementos of $[tabla]">
                  <ion-select-option [value]="elementos.ID">{{elementos.Name}}</ion-select-option>
                </div>
              </ion-select>
            </ion-item>
			</ion-col>
			`
			memory = eRP(memory, "$[name]", eRP(dat.ColName, "ID", ""))
			memory = eRP(memory, "$[tabla]", dat.data.Table)
			html += memory
		}
	}
	return html
}

func htmAllGetFIltros(val []Valores) string {
	html := ""
	for _, dat := range eNV(val) {
		if dat.Fk == true {
			memory := `
	<ion-item>
			<ion-label position="floating">
					<ion-icon slot="start" name="logo-buffer"></ion-icon>
					$[model]
			</ion-label>
			<ion-select (ionChange)="CalcularFiltro()" [(ngModel)]="F$[model]ID" okText="Aceptar" cancelText="Cancelar">
					<ion-select-option [value]="0">Todas</ion-select-option>
					<ion-select-option *ngFor="let elementos of $[table]" [value]="elementos.ID">{{elementos.Name}}</ion-select-option>
			</ion-select>
	</ion-item>
`
			memory = eRP(memory, "$[model]", dat.data.Model)
			memory = eRP(memory, "$[table]", dat.data.Table)
			html += memory
		}
	}
	return html
}

func htmAllGetFiltrosTs(val []Valores) string {
	vars := ""
	functionData := ""

	for _, dat := range eNV(val) {
		if dat.Fk == true {
			// Get vars
			v := `
public $[table]: $[modelo]Model[];
public F$[name]: Number = 0;
`
			v = eRP(v, "$[table]", dat.data.Table)
			v = eRP(v, "$[name]", dat.ColName)
			v = eRP(v, "$[modelo]", dat.data.Model)
			// Get function fata
			f := `
    if (this.F$[name] !== 0) {
      a = this.service.FIGual(a, '$[name]', this.F$[name]);
    }
`
			f = eRP(f, "$[name]", dat.ColName)

			vars += v
			functionData += f
		}
	}

	html := `
// Variables filtro
$[vars]
  /**
   * CalcularFiltro
   */
  public CalcularFiltro() {
    console.log('Calcular Filtro');
    let a = this.elementosBase;
$[func]
    this.elementos = a;
  }
`
	html = eRP(html, "$[vars]", vars)
	html = eRP(html, "$[func]", functionData)

	return html
}
