module gitlab.com/AndrusGerman/AngularGenerate

go 1.14

require (
	github.com/iancoleman/strcase v0.0.0-20191112232945-16388991a334
	github.com/jinzhu/gorm v1.9.14
	github.com/jinzhu/inflection v1.0.0
)
