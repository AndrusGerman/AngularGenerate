package main

import (
	"flag"
	"fmt"
	"time"

	"github.com/jinzhu/gorm"
)

func init() {
	flag.StringVar(&database, "dbname", "", "Usage For Db Name")
	flag.StringVar(&DBhost, "host", "", "Host 'localhost','12.22.22'")
	flag.StringVar(&DBUser, "userdb", "", "User Database")
	flag.StringVar(&DBPassword, "paswdb", "", "Databse Password")
	flag.StringVar(&AngularGenerateOutput, "o", eParseOS("output/"), "Output Folder")
	flag.Parse()
}

func main() {
	eClearScreen()
	fmt.Printf("Iniciando app:\n\n")
	//Variables DB
	if database == "" {
		database = getDefault("DATABASE?", "appinventory")
	}
	if DBhost == "" {
		DBhost = getDefault("HOST?", "localhost")
	}
	if DBUser == "" {
		DBUser = getDefault("USER DB?", "postgres")
	}
	if DBPassword == "" {
		DBPassword = getDefault("PASSWORD DB?", "postgres")
	}

	// Connection to DB
	db := DbGetConnection()
	defer db.Close()
	// Create Folder
	eCreateFolder("")
	// Generate Files
	for {
		//Agregar Un paquete
		v := get("Deseas agregar un paquete? Enter=yes, Other=no:")
		if v == "" {
			//Get Data
			getData()
			generarData(db)
		} else {
			return
		}
		time.Sleep(5 * time.Second)
	}
}

func getData() {
	// Get Package
	for {
		var pk = get("Ingrese nombre del paquete: package-")
		if pk != "" {
			packageName = "package-" + pk
			break
		} else {
			println("\nPackage Name Is Null error")
		}
	}
	// Get Schema
	for {
		DBschema = get("Ingrese el Schema: ")
		if DBschema != "" {
			break
		} else {
			println("\nSchema Name Is Null error")
		}
	}
	eClearScreen()
}

func generarData(db *gorm.DB) {
	//	Imprimo
	fmt.Printf("PKG:%s , SCH:%s , DB:%s\n", packageName, DBschema, database)
	// Recorrer
	var imp, declaration string
	for _, val := range getTablesAndValues(db, DBschema) {
		eClearScreen()
		// if get("Agregar '"+val.TableNameModel+"' en el paquete? enter add,other delete:") == "" {
		a, b := generateByTable(val)
		imp += a
		declaration += b
		//}
	}
	fmt.Printf("IMPORTACIONES\n\n%s", imp)
	fmt.Printf("\n\nDECLARACIONES\n\n%s", declaration)
}
