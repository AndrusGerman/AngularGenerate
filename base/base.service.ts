import { Injectable } from '@angular/core';
import { RestService } from '../../rest.service';
import { Observable,throwError } from 'rxjs';
import { $[model]Model } from './$[tablesingular-]';

@Injectable({
  providedIn: 'root'
})
export class $[model]Service {
  private ServicePort: Number = 8082;
  constructor(
    private rest: RestService
  ) { }
  public Create(el: $[model]Model) {
    this.rest.puerto = this.ServicePort;
    return <Observable<$[model]Model>>this.rest.Post('$[table]', el);
  }
  public Update($[model]ModelID: any, el: $[model]Model) {
    this.rest.puerto = this.ServicePort;
    return <Observable<$[model]Model>>this.rest.Put(`$[table]/${$[model]ModelID}`, el);
  }
  public Delete(ID: any) {
    this.rest.puerto = this.ServicePort;
    return <Observable<any>>this.rest.Delete(`$[table]/${ID}`);
  }
  public GetByID(ID: any) {
    this.rest.puerto = this.ServicePort;
    return <Observable<$[model]Model>>this.rest.Get(`$[table]/${ID}`);
  }
  public GetAll() {
    this.rest.puerto = this.ServicePort;
    return <Observable<$[model]Model[]>>this.rest.Get(`$[table]`);
  }
  public GetAllByCompanyID(CompanyID: any) {
    this.rest.puerto = this.ServicePort;
    return <Observable<$[model]Model[]>>this.rest.Get(`$[table]_company/${CompanyID}`);
  }

  public UpdateImage($[model]ID: any, file: File) {
    if (file == null) {
      return throwError('Error Imagen Nula');
    }
    this.rest.puerto = this.ServicePort;
    return this.rest.UploadOneFile(`$[table]_image/${$[model]ID}`, file, 'file');
  }

  // Extra
  public publicbase(): string {
    return `${this.rest.base}:${this.ServicePort}/public/`;
  }
  /**
   * FIGual
   */
  public FIGual(todos: $[model]Model[], campo: string, dato: any): $[model]Model[] {
    const ret: $[model]Model[] = [];
    todos.forEach(element => {
      if (element[campo] === dato) {
        ret.push(element);
      }
    });
    return ret;
  }
}
