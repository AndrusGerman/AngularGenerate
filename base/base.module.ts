import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { PackageAuthPage } from './package-auth.page';
import { ReactiveFormsModule } from '@angular/forms';


import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { MemberRoutingModule } from './login/member-routing/member-routing.module';
import { HomeMasterComponent } from './home-master/home-master.component';

import { EditAppRoleUserComponent } from './app-role-user/edit-app-role-user/edit-app-role-user.component';
import { ViewAppRoleUserComponent } from './app-role-user/view-app-role-user/view-app-role-user.component';
import { AddAppRoleUserComponent } from './app-role-user/add-app-role-user/add-app-role-user.component';
import { AllAppRoleUserComponent } from './app-role-user/all-app-role-user/all-app-role-user.component';
import { EditRoleUserComponent } from './role-user/edit-role-user/edit-role-user.component';
import { ViewRoleUserComponent } from './role-user/view-role-user/view-role-user.component';
import { AddRoleUserComponent } from './role-user/add-role-user/add-role-user.component';
import { AllRoleUserComponent } from './role-user/all-role-user/all-role-user.component';
import { EditCompanyFollowerComponent } from './company-follower/edit-company-follower/edit-company-follower.component';
import { ViewCompanyFollowerComponent } from './company-follower/view-company-follower/view-company-follower.component';
import { AddCompanyFollowerComponent } from './company-follower/add-company-follower/add-company-follower.component';
import { AllCompanyFollowerComponent } from './company-follower/all-company-follower/all-company-follower.component';
import { EditMenuComponent } from './menu/edit-menu/edit-menu.component';
import { ViewMenuComponent } from './menu/view-menu/view-menu.component';
import { AddMenuComponent } from './menu/add-menu/add-menu.component';
import { AllMenuComponent } from './menu/all-menu/all-menu.component';
import { EditAppComponent } from './app/edit-app/edit-app.component';
import { ViewAppComponent } from './app/view-app/view-app.component';
import { AddAppComponent } from './app/add-app/add-app.component';
import { AllAppComponent } from './app/all-app/all-app.component';
import { EditRoleAppComponent } from './role-app/edit-role-app/edit-role-app.component';
import { ViewRoleAppComponent } from './role-app/view-role-app/view-role-app.component';
import { AddRoleAppComponent } from './role-app/add-role-app/add-role-app.component';
import { AllRoleAppComponent } from './role-app/all-role-app/all-role-app.component';
import { EditGuardComponent } from './guard/edit-guard/edit-guard.component';
import { ViewGuardComponent } from './guard/view-guard/view-guard.component';
import { AddGuardComponent } from './guard/add-guard/add-guard.component';
import { AllGuardComponent } from './guard/all-guard/all-guard.component';
import { EditPermissionComponent } from './permission/edit-permission/edit-permission.component';
import { ViewPermissionComponent } from './permission/view-permission/view-permission.component';
import { AddPermissionComponent } from './permission/add-permission/add-permission.component';
import { AllPermissionComponent } from './permission/all-permission/all-permission.component';
import { EditRoleComponent } from './role/edit-role/edit-role.component';
import { ViewRoleComponent } from './role/view-role/view-role.component';
import { AddRoleComponent } from './role/add-role/add-role.component';
import { AllRoleComponent } from './role/all-role/all-role.component';
import { EditMenuPermissionRoleComponent } from './menu-permission-role/edit-menu-permission-role/edit-menu-permission-role.component';
import { ViewMenuPermissionRoleComponent } from './menu-permission-role/view-menu-permission-role/view-menu-permission-role.component';
import { AddMenuPermissionRoleComponent } from './menu-permission-role/add-menu-permission-role/add-menu-permission-role.component';
import { AllMenuPermissionRoleComponent } from './menu-permission-role/all-menu-permission-role/all-menu-permission-role.component';
import { EditAppTypeComponent } from './app-type/edit-app-type/edit-app-type.component';
import { ViewAppTypeComponent } from './app-type/view-app-type/view-app-type.component';
import { AddAppTypeComponent } from './app-type/add-app-type/add-app-type.component';
import { AllAppTypeComponent } from './app-type/all-app-type/all-app-type.component';
import { EditMenuRoleComponent } from './menu-role/edit-menu-role/edit-menu-role.component';
import { ViewMenuRoleComponent } from './menu-role/view-menu-role/view-menu-role.component';
import { AddMenuRoleComponent } from './menu-role/add-menu-role/add-menu-role.component';
import { AddMenuRoleComponent } from '.';
import { AllMenuRoleComponent } from './menu-role/all-menu-role/all-menu-role.component';
import { EditMenuServiceComponent } from './menu-service/edit-menu-service/edit-menu-service.component';
import { ViewMenuServiceComponent } from './menu-service/view-menu-service/view-menu-service.component';
import { AddMenuServiceComponent } from './menu-service/add-menu-service/add-menu-service.component';
import { AllMenuServiceComponent } from './menu-service/all-menu-service/all-menu-service.component';
import { EditPermissionRoleComponent } from './permission-role/edit-permission-role/edit-permission-role.component';
import { ViewPermissionRoleComponent } from './permission-role/view-permission-role/view-permission-role.component';
import { AddPermissionRoleComponent } from './permission-role/add-permission-role/add-permission-role.component';
import { AllPermissionRoleComponent } from './permission-role/all-permission-role/all-permission-role.component';
import { EditUserAddressComponent } from './user-address/edit-user-address/edit-user-address.component';
import { ViewUserAddressComponent } from './user-address/view-user-address/view-user-address.component';
import { AddUserAddressComponent } from './user-address/add-user-address/add-user-address.component';
import { AllUserAddressComponent } from './user-address/all-user-address/all-user-address.component';
import { EditUserComponent } from './user/edit-user/edit-user.component';
import { ViewUserComponent } from './user/view-user/view-user.component';
import { AddUserComponent } from './user/add-user/add-user.component';
import { AllUserComponent } from './user/all-user/all-user.component';
import { EditPlanComponent } from './plan/edit-plan/edit-plan.component';
import { ViewPlanComponent } from './plan/view-plan/view-plan.component';
import { AddPlanComponent } from './plan/add-plan/add-plan.component';
import { AllPlanComponent } from './plan/all-plan/all-plan.component';
import { EditServiceComponent } from './service/edit-service/edit-service.component';
import { ViewServiceComponent } from './service/view-service/view-service.component';
import { AddServiceComponent } from './service/add-service/add-service.component';
import { AllServiceComponent } from './service/all-service/all-service.component';


const routes: Routes = [
  {
    path: '',
    component: PackageAuthPage
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'client',
    loadChildren: './home-client/home-client.module#HomeClientPageModule'
  },
  {
    path: 'master',
    component: HomeMasterComponent
  }

];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    MemberRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [PackageAuthPage,
    LoginComponent,
    HomeMasterComponent,
    EditAppRoleUserComponent,
    ViewAppRoleUserComponent,
    AddAppRoleUserComponent,
    AllAppRoleUserComponent,
    EditRoleUserComponent,
    ViewRoleUserComponent,
    AddRoleUserComponent,
    AllRoleUserComponent,
    EditCompanyFollowerComponent,
    ViewCompanyFollowerComponent,
    AddCompanyFollowerComponent,
    AllCompanyFollowerComponent,
    EditMenuComponent,
    ViewMenuComponent,
    AddMenuComponent,
    AllMenuComponent,
    EditAppComponent,
    ViewAppComponent,
    AddAppComponent,
    AllAppComponent,
    EditRoleAppComponent,
    ViewRoleAppComponent,
    AddRoleAppComponent,
    AllRoleAppComponent,
    EditGuardComponent,
    ViewGuardComponent,
    AddGuardComponent,
    AllGuardComponent,
    EditPermissionComponent,
    ViewPermissionComponent,
    AddPermissionComponent,
    AllPermissionComponent,
    EditRoleComponent,
    ViewRoleComponent,
    AddRoleComponent,
    AllRoleComponent,
    EditMenuPermissionRoleComponent,
    ViewMenuPermissionRoleComponent,
    AddMenuPermissionRoleComponent,
    AllMenuPermissionRoleComponent,
    EditAppTypeComponent,
    ViewAppTypeComponent,
    AddAppTypeComponent,
    AllAppTypeComponent,
    EditMenuRoleComponent,
    ViewMenuRoleComponent,
    AddMenuRoleComponent,
    AllMenuRoleComponent,
    EditMenuServiceComponent,
    ViewMenuServiceComponent,
    AddMenuServiceComponent,
    AllMenuServiceComponent,
    EditPermissionRoleComponent,
    ViewPermissionRoleComponent,
    AddPermissionRoleComponent,
    AllPermissionRoleComponent,
    EditUserAddressComponent,
    ViewUserAddressComponent,
    AddUserAddressComponent,
    AllUserAddressComponent,
    EditUserComponent,
    ViewUserComponent,
    AddUserComponent,
    AllUserComponent,
    EditPlanComponent,
    ViewPlanComponent,
    AddPlanComponent,
    AllPlanComponent,
    EditServiceComponent,
    ViewServiceComponent,
    AddServiceComponent,
    AllServiceComponent,
  ],
  exports: []
})
export class PackageAuthPageModule { }
