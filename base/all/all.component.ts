import { Component, OnInit } from '@angular/core';
import { $[model]Model } from '../$[tablesingular-]';
import { $[model]Service } from '../$[tablesingular-].service';
import { DomSanitizer } from '@angular/platform-browser';


$[import]

// Presentacion
import { ActionSheetController, NavController } from '@ionic/angular';

@Component({
  selector: 'all-$[tablesingular-]',
  templateUrl: './all-$[tablesingular-].component.html',
  styleUrls: ['./all-$[tablesingular-].component.scss']
})
export class All$[model]Component implements OnInit {
  constructor(
    private service: $[model]Service,
    public actionSheetController: ActionSheetController,
    public nav: NavController,
    protected _sanitizer: DomSanitizer,
$[serviceconstructor]
  ) { }

  // Todo los elementos
  public elementosBase: $[model]Model[];
  // Elementos a mostrar
  public elementos: $[model]Model[];

  // 0 NormalAll, 1 Agregar, 2 Editar, 3 Visualizar
  public modo: Number = 0;
  public select_element = new ($[model]Model);

$[filtro]

  async eliminarQuest(item: $[model]Model) {
    this.modo = 0;
    const actionSheet = await this.actionSheetController.create({
      // header: 'Desea eliminar ' + item.Name,
      header: 'Desea eliminar el elemento selecionado',
      buttons: [
        { text: 'Eliminar', role: 'destructive',
          icon: 'trash',
          handler: () => {
            this.eliminar(item);
            this.eliminarDB(item);
          }
        },
        {
          text: 'Cancel', icon: 'close', role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }]
    });
    await actionSheet.present();
  }


  public eliminar(item: $[model]Model) {
    const index = this.elementos.indexOf(item);
    this.elementos.splice(index, 1);
  }
  public eliminarDB(item: $[model]Model) {
    this.service.Delete(item.ID).subscribe(v => {});
  }
  /**
   * GetElementos
   */
  public GetElementos() {
$[getall]
  }

  /**
   * view
   */
  public view(item: $[model]Model) {
    this.service.GetByID(item.ID).subscribe(v => {
      this.select_element = v;
      this.modo = 3;
    });
  }
  ngOnInit() {
    // Prepare
    const app = this;
    // this.companyService.GetNow().subscribe(v => {
    //   this.branchService.GetAllByCompanyID(v.ID).subscribe(br => {
    //     this.Branches = br;
    //   });
    // });

$[oninit]
    // Run
    this.GetElementos();
  }

  /**
   * ImageStyle
   */
  public ImageStyle(url) {
    const style = 'background-size: cover;background-image: url(' + url + ')';
    return this._sanitizer.bypassSecurityTrustStyle(style);
  }
}
