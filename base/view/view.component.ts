import { Component, OnInit, Input } from '@angular/core';
// Models
import { $[model]Model } from '../$[tablesingular-]';
// Service
import { $[model]Service } from '../$[tablesingular-].service';

@Component({
  selector: 'app-view-$[tablesingular-]',
  templateUrl: './view-$[tablesingular-].component.html',
  styleUrls: ['./view-$[tablesingular-].component.scss']
})
export class View$[model]Component implements OnInit {
  @Input() item: $[model]Model = new($[model]Model);
  constructor(
    private service: $[model]Service
  ) { }

  ngOnInit() {
  }
}
