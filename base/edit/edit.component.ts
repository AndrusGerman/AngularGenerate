import { Component, OnInit, Input } from '@angular/core';
import { $[model]Model } from '../$[tablesingular-]';
import { $[model]Service } from '../$[tablesingular-].service';
import { All$[model]Component } from '../all-$[tablesingular-]/all-$[tablesingular-].component';


$[import]

@Component({
  selector: 'app-edit-$[tablesingular-]',
  templateUrl: './edit-$[tablesingular-].component.html',
  styleUrls: ['./edit-$[tablesingular-].component.scss']
})
export class Edit$[model]Component implements OnInit {
  @Input() item: $[model]Model = new ($[model]Model);
  constructor(
    private service: $[model]Service,
    private allComponet: All$[model]Component,
$[serviceconstructor]
  ) { }
  // Datos para el formulario
  // public company: Company = new (Company);
$[vars]
  public Image: File;
  // // Datos de las imagenes
  // // Save Image
  public changeImage($event: any) {
    this.Image = $event.target.files[0];
  }
  public press_button(name: string) {
    document.getElementById(name).click();
  }
  // Crear el elemento
  public create() {
    // Agregar
    const app = this;
    // Crear
    this.service.Update(app.item.ID, app.item).subscribe({
      next(v) {
        app.SendImage(v.ID);
      },
      error(v) {
      },
    });
  }

  // Upload New Image
  public SendImage(id) {
    const app = this;
    // this.service.UpdateImage(id, app.Image).subscribe({
    //   next(siguiente) { app.RefresItem(id); },
    //   error(siguiente) { app.RefresItem(id); }
    // });
    app.RefresItem(id);
  }

  /**
   * RefresItem
   */

  public RefresItem(id) {
    const app = this;
    // Refresh
    app.allComponet.GetElementos();
    app.allComponet.modo = 0;
  }

  ngOnInit() {
    // Get Ultimate Change
    this.service.GetByID(this.item.ID).subscribe(v => {
      this.item = v;
    });
    // // Get Data para el formulario
    // this.companyService.GetNow().subscribe(company => {
    //   this.company = company;
    //   this.branchService.GetAllByCompanyID(company.ID).subscribe(branch => {
    //     this.sucursales = branch;
    //   });
    // });

$[oninit]
  }
}
