import { Component, OnInit } from '@angular/core';
import { $[model]Model } from '../$[tablesingular-]';
import { $[model]Service } from '../$[tablesingular-].service';
import { All$[model]Component } from '../all-$[tablesingular-]/all-$[tablesingular-].component';

$[import]



@Component({
  selector: 'app-add-$[tablesingular-]',
  templateUrl: './add-$[tablesingular-].component.html',
  styleUrls: ['./add-$[tablesingular-].component.scss']
})
export class Add$[model]Component implements OnInit {
  constructor(
    private service: $[model]Service,
    private allComponet: All$[model]Component,

$[serviceconstructor]
  ) { }
  public item: $[model]Model = new ($[model]Model);
  // Datos para el formulario
$[vars]
  public Image: File;
  // // Save Image
  public changeImage($event: any) {
    this.Image = $event.target.files[0];
  }
  public press_button(name: string) {
    document.getElementById(name).click();
  }
  // Crear el elemento
  public create($event: any) {
    // Agregar
    const app = this;
    // this.item.CompanyID = this.company.ID;
    // Crear
    this.service.Create(this.item).subscribe({
      next(v) {
        app.SendImage(v.ID);
      },
      error(v) {
        alert(JSON.stringify(v));
      },
    });
  }

  // Upload New Image
  public SendImage(id) {
    const app = this;
    // this.service.UpdateImage(id, this.Image).subscribe({
    //   next(siguiente) { app.RefresItem(id); },
    //   error(siguiente) { app.RefresItem(id); }
    // });
    app.RefresItem(id);
  }

  /**
   * RefresItem
   */
  public RefresItem(id) {
    const app = this;
    // Refresh
    app.service.GetByID(id).subscribe(el => {
      app.allComponet.elementosBase.push(el);
      app.allComponet.elementosBase.reverse();
      app.allComponet.CalcularFiltro();
      app.allComponet.modo = 0;
    });
  }


  ngOnInit() {
    // Get Data para el formulario
    // this.companyService.GetNow().subscribe(company => {
    //   this.item.CompanyID = company.ID;
    // });
$[oninit]
  }
}
