package main

import (
	"io/ioutil"
	"os"
	"runtime"
	"strings"
)

func eRP(all, old, newstring string) string {
	return strings.Replace(all, old, newstring, 1000)
}

func eP(all string) string {
	return strings.Replace(all, "_", "-", 6999)
}

func eCreateFolder(name string) error {
	dir := AngularGenerateOutput + name
	return os.MkdirAll(eParseOS(dir), 0777)
}

func eCreateFile(name string, text string) error {
	dir := AngularGenerateOutput + name[2:]
	return ioutil.WriteFile(eParseOS(dir), []byte(text), 0777)
}

func eNameTableFile(data Tabla, fileType string, forr string) string {
	return "./" + packageName + "/" + eP(data.TableNameSingular) + "/" + forr + "-" + eP(data.TableNameSingular) + "/" + forr + "-" + eP(data.TableNameSingular) + ".component." + fileType
}
func eReadFileT(crud string, fileType string) (string, error) {
	return eReadFile(crud + "/" + crud + ".component." + fileType)
}

func eReadFile(name string) (string, error) {
	a, err := ioutil.ReadFile(eParseOS("./base/" + name))
	return string(a), err
}

func eNV(val []Valores) []Valores {
	var re []Valores
	for _, t := range val {
		if t.ColName != "ID" {
			re = append(re, t)
		}
	}
	return re
}

func eClearScreen() {
	value, ok := clear[runtime.GOOS]
	if ok {
		value()
	}
}
func eParseOS(dirUnix string) string {
	if runtime.GOOS == "linux" {
		return dirUnix
	}
	return eRP(dirUnix, "/", `\`)
}

func eParseModelsService(ts string) string {
	for dataOld, DataNew := range ModelCall {
		ts = eRP(ts, dataOld+"Model", DataNew+"Model")
		ts = eRP(ts, dataOld+"Service", DataNew+"Service")
	}
	return ts
}

func eGetImportsFK(val []Valores) string {
	re := ""
	for _, v := range val {
		if v.data.Model != "Company" {
			if v.Fk { //Agrega los imports
				if packageDB[v.data.Model] == "" {
					model := eRP(v.ColName, "ID", "")
					im := `
import { $[model]Model } from '../../../$[package]/$[tablesingular]/$[tablesingular]';
import { $[model]Service } from '../../../$[package]/$[tablesingular]/$[tablesingular].service';
`
					// Packages date
					md := get("El modelo de '" + model + "'? Enter '" + model + "': ")
					if md == "" {
						im = eRP(im, "$[model]", model)
					} else {
						im = eRP(im, "$[model]", md)
						ModelCall[model] = md

					}
					// Packages date
					pk := get("Cual es el paquete de '" + v.data.Model + "'? Enter '" + packageName + "':")
					if pk == "" {
						im = eRP(im, "$[package]", packageName)
					} else {
						im = eRP(im, "$[package]", pk)
					}

					// import tables
					tbs := get("Cual es la tabla de '" + v.data.Model + "'? Enter '" + eP(v.data.TableSingular) + "':")
					if tbs == "" {
						im = eRP(im, "$[tablesingular]", eP(v.data.TableSingular))
					} else {
						im = eRP(im, "$[tablesingular]", eP(tbs))
					}

					re += im
					packageDB[v.data.Model] = im
				} else {
					re += packageDB[v.data.Model]
				}

			}
		} else {
			im := `
import { CompanyModel } from '../../../package-company/company/company';
import { CompanyService } from '../../../package-company/company/company.service';
`
			re += im
		}
	}
	re = eP(re)
	return re
}
func eGetVariablesFK(val []Valores) string {
	re := ""
	for _, v := range val {
		if v.Fk { //Agrega los imports
			model := eRP(v.ColName, "ID", "")
			re += "public " + v.data.Table + ": " + model + "Model[];\n"
		}
		if v.ColName == "Logo" {
			re += "public " + v.ColName + ": File;\n"
		}
	}
	return re
}

func eGetPrepareVarsFK(val []Valores) string {
	re := ""
	for _, v := range val {
		if v.Fk {
			// OnInit Prepare
			on := `
    this.$[tablesingular]Service.GetAll().subscribe(v => {
      this.$[table] = v;
    });
`
			on = eRP(on, "$[tablesingular]", v.data.TableSingular)
			on = eRP(on, "$[table]", v.data.Table)
			re += on
		}
	}
	return re
}

func eGetServiceConstructorFK(val []Valores) string {
	re := ""
	for _, v := range val {
		if v.Fk {
			model := eRP(v.ColName, "ID", "")
			re += "    public " + v.data.TableSingular + "Service: " + model + "Service,\n"
		}
	}
	return re
}

func eGetAllDataByCompanyIDTS(tabla Tabla) string {
	if eModelByAny(tabla.TableNameModel) {
		return `
    this.companyService.GetNow().subscribe(company => {
      this.service.GetAllByCompanyID(company.ID).subscribe(elementos => {
        this.elementosBase = elementos.reverse();
        this.CalcularFiltro();
      });
    });
`
	}
	return `
    this.service.GetAll().subscribe(elementos => {
      this.elementosBase = elementos.reverse();
      this.CalcularFiltro();
    });
`
}

func eModelByAny(model string) bool {
	if TableByAny[model] == "s" {
		return true
	}
	if TableByAny[model] == "" {
		a := get("Obtener '" + model + "' by company ID? Enter:NO , Any=Si :")
		if a != "" {
			TableByAny[model] = "s"
			return true
		}
		TableByAny[model] = "n"
		return false
	}
	return false
}
