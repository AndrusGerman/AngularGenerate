package main

func createView(table Tabla) {
	viewscss(table)
	viewHTML(table)
	viewTS(table)
}

func viewTS(table Tabla) {
	base, _ := eReadFileT("view", "ts")
	base = eRP(base, "$[tablesingular]", table.TableNameSingular)
	base = eRP(base, "$[model]", table.TableNameModel)
	base = eRP(base, "$[tablesingular-]", eP(table.TableNameSingular))
	base = eParseModelsService(base)
	eCreateFile(eNameTableFile(table, "ts", "view"), base)
}

func viewHTML(table Tabla) {
	base, _ := eReadFileT("view", "html")
	base = eRP(base, "$[tablesingular-]", eP(table.TableNameSingular))
	eCreateFile(eNameTableFile(table, "html", "view"), base)
}

func viewscss(table Tabla) {
	base, _ := eReadFileT("view", "scss")
	eCreateFile(eNameTableFile(table, "scss", "view"), base)
}
