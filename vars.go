package main

import (
	"os"
	"os/exec"
)

var packageName string

// DBschema scheme database
var DBschema string
var database string

// DBhost ip-host database
var DBhost string

// DBUser user database
var DBUser string

// DBPassword password database
var DBPassword string

var packageDB = make(map[string]string)

// TableByAny this  search by any element ID
var TableByAny = make(map[string]string)

// ModelCall call real name model
var ModelCall = make(map[string]string)

var clear map[string]func() //create a map for storing clear funcs

// AngularGenerateOutput folder
var AngularGenerateOutput string

func init() {
	clear = make(map[string]func()) //Initialize it
	clear["linux"] = func() {
		cmd := exec.Command("clear") //Linux example, its tested
		cmd.Stdout = os.Stdout
		cmd.Run()
	}
	clear["windows"] = func() {
		cmd := exec.Command("cmd", "/c", "cls") //Windows example, its tested
		cmd.Stdout = os.Stdout
		cmd.Run()
	}
}
