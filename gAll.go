package main

func createAll(table Tabla) {
	allscss(table)
	allHTML(table)
	allTS(table)
}

func allTS(table Tabla) {
	base, _ := eReadFileT("all", "ts")

	imports := eGetImportsFK(table.Valores)
	variables := eGetVariablesFK(table.Valores)
	onInit := eGetPrepareVarsFK(table.Valores)
	serviceConstructor := eGetServiceConstructorFK(table.Valores)
	filtro := htmAllGetFiltrosTs(table.Valores)
	base = eRP(base, "$[model]", table.TableNameModel)
	base = eRP(base, "$[import]", imports)
	base = eRP(base, "$[vars]", variables)
	base = eRP(base, "$[tablesingular]", table.TableNameSingular)
	base = eRP(base, "$[tablesingular-]", eP(table.TableNameSingular))
	base = eRP(base, "$[serviceconstructor]", serviceConstructor)
	base = eRP(base, "$[oninit]", onInit)
	base = eRP(base, "$[filtro]", filtro)
	base = eRP(base, "$[getall]", eGetAllDataByCompanyIDTS(table))
	base = eParseModelsService(base)

	eCreateFile(eNameTableFile(table, "ts", "all"), base)
}

func allHTML(table Tabla) {
	base, _ := eReadFileT("all", "html")
	filtros := htmAllGetFIltros(table.Valores)
	base = eRP(base, "$[filtros]", filtros)
	base = eRP(base, "$[tablesingular-]", eP(table.TableNameSingular))
	eCreateFile(eNameTableFile(table, "html", "all"), base)
}

func allscss(table Tabla) {
	base, _ := eReadFileT("all", "scss")

	eCreateFile(eNameTableFile(table, "scss", "all"), base)
}
