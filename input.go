package main

import (
	"fmt"
)

func get(msg string) string {
	var data string
	fmt.Print(msg)
	fmt.Scanln(&data)
	data = eRP(data, "\n", "")
	return data
}

func getDefault(msg string, dataDefault string) string {
	var data string
	fmt.Printf("%s ; ENTERN ON BLANK '%s'?: ", msg, dataDefault)
	fmt.Scanln(&data)
	data = eRP(data, "\n", "")
	if data == "" {
		data = dataDefault
	}
	return data
}
