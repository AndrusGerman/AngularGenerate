package main

func createService(table Tabla) {
	base, _ := eReadFile("base.service.ts")
	base = eRP(base, "$[model]", table.TableNameModel)
	base = eRP(base, "$[tablesingular]", table.TableNameSingular)
	base = eRP(base, "$[tablesingular-]", eP(table.TableNameSingular))
	base = eRP(base, "$[table]", table.TableName)
	fileName := "./" + packageName + "/" + eP(table.TableNameSingular) + "/" + eP(table.TableNameSingular) + ".service.ts"
	eCreateFile(fileName, base)
}
